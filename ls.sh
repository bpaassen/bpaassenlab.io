#!/bin/sh -e
#
# ls -- List Site (because the web doesn't have one)
#
# Copyright (C) 2017-2019  Jochen Sprickerhof
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# This is a project fork for Benjamin Paassen's website.
# Metdata changes applied by Benjamin Paassen, 2019.

IN="site"
OUT="../public/"
SITENAME="Benjamin Paassen"
DESCRIPTION="Benjamin Paassen's home page"
KEYWORDS="Benjamin Paassen, Machine Learning, Metric Learning, Structured Data, Edit Distances, Educational Datamining, Intelligent Tutoring Systems, Bionic Prostheses, Transfer Learning, Video Game Culture, Algorithmic Fairness"
LANG="en"

list() {
	[ "$(find "$1"/ -maxdepth 1 -name "*.md" -o -name "*.bib" -o -name "*.lnk" | wc -l)" = 1 ] && return
	printf '<p>\n'
	[ "$1" = "" ] && printf '<a href="/">⌂</a>\n'
	for l in "$1"*; do
		case $l in
			*index.*)
				;;
			*.lnk)
				printf '<a href="%s">%s→</a>\n' "$(cat "$l")" "$(basename "$l" .lnk)"
				;;
			"$s")
				case "$s" in
					*.md)
						printf '<a href="/%s" id="thisSite">%s%s</a>\n' "$([ -d "$l" ] && echo "$l"/ || echo "${l%md}html")" "$(basename "$l" .md)" "$([ -d "$l" ] && echo / || echo )"
						;;
					*.bib)
						printf '<a href="/%s" id="thisSite">%s%s</a>\n' "$([ -d "$l" ] && echo "$l"/ || echo "${l%bib}html")" "$(basename "$l" .bib)" "$([ -d "$l" ] && echo / || echo )"
						;;
				esac
				;;
			*.md)
				printf '<a href="/%s">%s</a>\n' "${l%md}html" "$(basename "$l" .md)"
				;;
			*.bib)
				printf '<a href="/%s">%s</a>\n' "${l%bib}html" "$(basename "$l" .bib)"
				;;
			*)
				[ -d "$l" ] && printf '<a href="/%s">%s/</a>\n' "$l" "$(basename "$l")"
				;;
		esac
	done
	printf '</p>\n'
}

html() {
	printf '<!DOCTYPE html><html lang="%s"><head>\n<meta charset="utf-8">\n' "$LANG"
	case "$s" in
		index.md)
			printf '<title>%s</title>\n' "$SITENAME"
			;;
		*/index.*)
			printf '<title>%s | %s</title>\n' "$(basename "$1")" "$SITENAME"
			;;
		*)
			printf '<title>%s | %s</title>\n' "$(basename "$s" .md)" "$SITENAME"
			;;
	esac
	printf '<link rel="stylesheet" type="text/css" href="/style.css">\n<meta name="viewport" content="width=device-width, initial-scale=1">\n'
	printf '<meta name="Description" content="%s">\n' "$DESCRIPTION"
	printf '<meta name="Keywords" content="%s">\n' "$KEYWORDS"
	printf '</head><body>\n<header><a href="/">%s</a></header>\n' "$SITENAME"
	printf '<nav>'
	printf '%s\n' "$2"
	list "$1"
	printf '</nav>'
	printf '<div id="changed">Last changed: %s</div>\n' "$(git log -1 --format="%ad" --date=short -- "$s")"
	printf '<main>\n'
	case "$s" in
		*.md)
			cat "$s"
			;;
		*.bib)
			../bib2md.py "$s"
			;;
	esac | markdown -ffencedcode
	printf '</main></body></html>\n'
}

site() {
	mkdir -p "$OUT$1"
	for s in "$1"*; do
		[ -d "$s" ] && site "$s"/ "$2$(list "$1")" && continue
		case $s in
			*.md)
				html "$@" > "$OUT${s%md}html"
				;;
			*.bib)
				html "$@" > "$OUT${s%bib}html"
				;;
			*.lnk)
				;;
			*)
				cp "$s" "$OUT$s"
				;;
		esac
	done
}

cd "$IN" && site
