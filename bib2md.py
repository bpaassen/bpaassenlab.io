#!/usr/bin/python3
#
# simple converter from Biblatex to Markdown
#
# Copyright (C) 2019  Jochen Sprickerhof
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from bibtexparser.bparser import BibTexParser
from bibtexparser.bwriter import BibTexWriter
from dateutil.parser import parse
from sys import argv


def main():
    parser = BibTexParser(ignore_nonstandard_types=False)
    writer = BibTexWriter()
    writer.entry_separator = ''

    bib = parser.parse_file(open(argv[1]))
    db = {}
    for e in bib.entries:

        if('date' in e):
            year = parse(e['date']).year
            e['year'] = str(year)
        elif('year' in e):
            year = int(e['year'])
        else:
            raise ValueError('publication does neither contain date nor year: %s' % str(e))
        db.setdefault(year, []).append(e)

    s = '  - {author} ({year}).  \n' \
        '    {title}.  \n' \
        '    *{booktitle}*{pages}{conf}.  '

    out = []
    for year in sorted(db.keys(), reverse=True):
        out.append(f'# {year}')

        elemOut = []
        for pub in db[year][::-1]:
            fmt = {}

            fmt['author'] = pub['author'].replace(' and', ',').replace('{', '').replace('}', '')

            fmt['year']  = pub['year']

            pub["title"] = pub["title"].replace('{', '').replace('}', '')
            fmt['title'] = f'**{pub["title"]}**'
            if 'subtitle' in pub:
                pub["subtitle"] = pub["subtitle"].replace('{', '').replace('}', '')
                fmt['title'] = f'**{pub["title"]}** {pub["subtitle"]}'

            if 'booktitle' in pub:
                fmt['booktitle'] = pub['booktitle'].replace('{', '').replace('}', '')
                if 'editor' in pub:
                  fmt['booktitle'] = 'In: ' + pub['editor'].replace(' and', ',').replace('{', '').replace('}', '') + ' (eds.) ' + fmt['booktitle']
            elif 'journal' in pub:
                fmt['booktitle'] = pub['journal'].replace('{', '').replace('}', '')
                if 'volume' in pub:
                  fmt['booktitle'] += ' ' + pub['volume']
                if 'number' in pub:
                  fmt['booktitle'] += ' (' + pub['number'] + ')'
            elif 'type' in pub:
                fmt['booktitle'] = pub['type']

            if 'pages' in pub:
                fmt['pages'] = ', pp. ' + pub['pages'].replace('--', '&ndash;')
            else:
                fmt['pages'] = ''

            fmt['conf'] = ', '.join([pub[k]
                                     for k in ('note', 'location', 'venue')
                                     if k in pub])

            if fmt['conf'] != '':
                fmt['conf'] = ', ' + fmt['conf']

            if('booktitle' not in fmt):
                raise ValueError('publication does contain neither booktitle, nor journal, nor type entry: %s' % str(pub))

            elemOut.append(s.format(**fmt))

            ref = []
            if 'file' in pub:
                ref.append(f'[[Paper]({pub["file"]})]')
            if 'doi' in pub:
                ref.append(f'[[doi:{pub["doi"]}](https://doi.org/{pub["doi"]})]')
            if 'url' in pub:
                ref.append(f'[[Link]({pub["url"]})]')
            if ref:
                elemOut.append('    {}.'.format(', '.join(ref)))

            elemOut.append(f'<details><summary>BibTeX</summary><pre>{writer._entry_to_bibtex(pub)}</pre></details>')

        out.append('\n'.join(elemOut))
    print('\n\n'.join(out))


if __name__ == '__main__':
    main()
