# Work Experience

=April 2023 - Today=
    **Junior Professor** (tenure track) for [Knowledge Representation and Machine Learning](https://www.uni-bielefeld.de/fakultaeten/technische-fakultaet/arbeitsgruppen/kml/) at Bielefeld University.
=Dec 2021 - Dec 2023=
    **Deputy head** of the [Educational Technology Lab](https://www.dfki.de/en/web/research/research-departments/educational-technology-lab/) supervised by [Prof. Niels Pinkwart](https://www.dfki.de/en/web/about-us/employee/person/nipi03/).
=Jun 2021 - April 2024=
    **Senior Researcher** at the *German Research Center for Artificial Intelligence* (DFKI), Germany, in the [Educational Technology Lab](https://www.dfki.de/en/web/research/research-departments/educational-technology-lab/) supervised by [Prof. Niels Pinkwart](https://www.dfki.de/en/web/about-us/employee/person/nipi03/). Funding provided by the [German Federal Ministry of Education and Research](https://www.bmbf.de/de/innovationswettbewerb-invite-11103.html) in the project [AI-supported personalization in job-related education (KIPerWeb)](https://www.dfki.de/web/forschung/projekte-publikationen/projekte-uebersicht/projekt/kiperweb).
=Jan 2021 - May 2021=
    **Research Affiliate** at *Humboldt-University of Berlin*, Germany, supervised by [Prof. Niels Pinkwart](https://www.informatik.hu-berlin.de/de/forschung/gebiete/cses/members/niels.pinkwart/).
    Funding provided by the [DFG](https://www.dfg.de/en/research_funding/programmes/individual/research_fellowships/) in the project [Teaching Computer Programming using Deep Recursive Neural Networks](https://gepris.dfg.de/gepris/projekt/424460920).
=Jan 2020 - Dec 2020=
    **Research Affiliate** at *The University of Sydney*, Australia, supervised by [Prof. Kalina Yacef](http://www.eng.usyd.edu.au/people/kalina.yacef.php) and [Prof. Irena Koprinska](http://www.eng.usyd.edu.au/people/kalina.yacef.php).
    Funding provided by the [DFG](https://www.dfg.de/en/research_funding/programmes/individual/research_fellowships/) in the project [Teaching Computer Programming using Deep Recursive Neural Networks](https://gepris.dfg.de/gepris/projekt/424460920).
=Jun 2019 - Dec 2019=
    **Researcher** at *Bielefeld University*, Germany.
    Member of the [Machine Learning Research Group](https://cit-ec.de/en/tcs).
    Funding provided by Bielefeld University in the [Bielefeld Young Researchers' Fund](https://www.uni-bielefeld.de/%28en%29/nachwuchs/promovierende/stipendien-und-foerderprogramme/nachwuchsfonds.html).
=May 2015 - May 2019=
    **Researcher** at *Bielefeld University*, Germany.
    Member of the [Machine Learning Research Group](https://cit-ec.de/en/tcs).
    Funding provided by the DFG from May 2015 to May 2018 in the project
    [Learning Dynamic Feedback for Intelligent Tutoring Systems (DynaFIT)](https://cit-ec.de/en/tcs/fit).
=Sep 2012 - Apr 2015=
    **Scientific Assistant** at *Bielefeld University*, Germany.
    Member of the [Machine Learning Research Group](https://cit-ec.de/en/tcs).
    Funding provided by the DFG in the project
    [Learning Feedback for Intelligent Tutoring Systems (FIT)](https://cit-ec.de/en/tcs/fit).
=Sep 2011 - Aug 2012=
    **Scientific Assistant** at *Bielefeld University*, Germany.
    Member of the [Practical Computer Science Group](https://www.techfak.uni-bielefeld.de/techfak/ags/pi/).

# Academic Education

=May 2015 - April 2019=
    **PhD Studies** in the doctoral programme [Intelligent Systems](https://cit-ec.de/en/graduate-school/doctoral-programme) at
    *Bielefeld University*, Germany supervised by [Prof. Barbara Hammer](https://www.techfak.uni-bielefeld.de/~bhammer/).
    Dissertation on ['Metric Learning for Structured Data'](http://doi.org/10.4119/unibi/2935545),
    graded 'excellent' (summa cum laude).
=Oct 2013 - Apr 2015=
    **Master Studies** in the graduate programme [Intelligent Systems](https://www.uni-bielefeld.de/%28en%29/technische-fakultaet/studiengaenge/MA_Intelligente_Systeme/) at
    *Bielefeld University*, Germany. Thesis on ['Adaptive Affine Sequence Alignment Using Algebraic Dynamic Programming'](https://pub.uni-bielefeld.de/record/2736686) supervised by [Prof. Barbara Hammer](https://www.techfak.uni-bielefeld.de/~bhammer/), graded 'very good' (1.0).
=Oct 2010 - Sep 2013=
    **Bachelor Studies** in the undergrad programme [Cognitive Informatics](https://www.uni-bielefeld.de/technische-fakultaet/studiengaenge/BA_Kognitive_Informatik/) at *Bielefeld University*, Germany. Thesis on 'Search Algorithms and their Application in BiBiServ2' supervised by [Prof. Robert Giegerich](https://www.techfak.uni-bielefeld.de/~robert/), graded 'very good' (1.0).

# Awards and Scholarships

= Jan 2024 and Jul 2024 =
    [Golden Chalk teaching award](https://fachschaft.techfak.de/lehrpreis) by the students' council of the faculty of technology of Bielefeld University for [Introduction to Data Mining](https://ekvv.uni-bielefeld.de/kvv_publ/publ/vd?id=426089509) and [User Studies in Interactive Intelligent Systems](https://ekvv.uni-bielefeld.de/kvv_publ/publ/vd?id=450572659).
= Jan 2024 - Dec 2027 =
    Member of the [Young College of the North Rhine-Westphalian Academy of Sciences, Humanities and the Arts](https://www.awk.nrw/foerderung/junges-kolleg) in the program [Humboldt<sup>n</sup>](https://humboldt-n.nrw/nachwuchsfoerderung/kolleg).
= Jan 2021 - May 2021 =
    **Reintegration Fellowship** of the [German Research Foundation (DFG)](https://www.dfg.de/en/research_funding/programmes/individual/research_fellowships/).
= Dec 2020 =
    **Winner (1st place)** of the [NeurIPS 2020 Education Challenge (Task 3)](https://eedi.com/projects/neurips-education-challenge) with Jessica McBroom.
= Oct 2020 - September 2025 =
    Junior Fellow of the [Center for Interdisciplinary Research](https://www.uni-bielefeld.de/\(en\)/ZiF/DJZ/fellows.html)
= Sep 2020 =
    ICML 2020 Top reviewer certificate.
= May 2020 =
    Nomination for the Doctoral Disseration Award of the International Neural Network Society (INNS).
= Jan 2020 =
    **Dissertation Prize** of the [University Society Bielefeld](https://50jahre.uni-bielefeld.de/en/2020/01/29/ausgezeichnet-die-besten-doktorarbeiten-aus-2019/).
= Jan 2020 - Dec 2020 =
    **Research Fellowship** of the [German Research Foundation (DFG)](https://www.dfg.de/en/research_funding/programmes/individual/research_fellowships/).
= Jun 2019 - Dec 2019 =
    **Postdoctoral Career Bridge Scholarship** of the [Bielefeld Young Researchers' Fund](https://www.uni-bielefeld.de/%28en%29/nachwuchs/promotionsinteressierte/stipendien-und-foerderprogramme/nachwuchsfonds.html).
= March 2019 =
    **Best Poster Award (2nd)** for the poster 'Unfair Feedback – On Long-Term Fairness of Automatic Decision Making With and Without Affirmative Action' at the [Interdisciplinary College (IK) 2019](https://interdisciplinary-college.de/).
= March 2018 =
    **Best Poster Award (3rd)** for the poster 'Greedy for Diversity: The Participant Partition Problem in IK Interaction Groups' at the [Interdisciplinary College (IK) 2018](https://interdisciplinary-college.de/).
= March 2017 =
    **Best Poster Award (2nd)** for the poster 'Transfer Learning for Robust Control of Bionic Prostheses' at the [Interdisciplinary College (IK) 2017](https://interdisciplinary-college.de/).
= Sep 2016 =
    **Best Presentation Award** for the paper ['Linear Supervised Transfer Learning for Generalized Matrix LVQ'](https://www.techfak.uni-bielefeld.de/~fschleif/mlr/mlr_04_2016.pdf) at the [New Challenges in Neural Computation Workshop (NC<sup>2</sup>) 2016](https://www.techfak.uni-bielefeld.de/~bhammer/GINN/NC2_2016/).
= Apr 2015 =
    **Best Student Paper Award** for the paper ['Adaptive structure metrics for automated
feedback provision in java programming'](http://www.elen.ucl.ac.be/Proceedings/esann/esannpdf/es2015-43.pdf) at the [European Symposium on Artificial Neural Networks (ESANN) 2015](https://www.elen.ucl.ac.be/esann/index.php?pg=esann15_programme).
= Apr 2014 =
    **Best Student Paper Award** for the paper ['Adaptive distance measures for sequential data'](http://www.elen.ucl.ac.be/Proceedings/esann/esannpdf/es2014-82.pdf) at the [European Symposium on Artificial Neural Networks (ESANN) 2014](https://www.elen.ucl.ac.be/esann/index.php?pg=esann14_programme).
= Feb 2011 - Apr 2015 =
    **Full Scholarship** of the [German Academic Scholarship Foundation ('Studienstiftung des deutschen Volkes')](https://www.studienstiftung.de/).

# Scientific Invited Talks

= September 2022 =
    **Keynote speaker** at the [Knowledge Sharing Festival](https://www.eventbrite.co.uk/e/knowledge-sharing-festival-2-tickets-399946780587?aff=website).
= September 2022 =
    **Keynote speaker** at the [DELFI workshop "Rahmenbedingungen für Künstliche Intelligenz in EdTech"](https://ki-campus.org/node/821).
= May 2021 =
    **Keynote speaker** at the [final event of the network for digital qualification in chemistry (DQC_Net)](https://www.youtube.com/watch?v=WRZju-j_CeM&t=2302s).
= Jan 2016 =
    **Invited Talk** at the [University of Ulm](https://www.uni-ulm.de/en/) on Metric Learning for Structured Data.

# Teaching & Guest lectures

= 2024 =
    **Lecturer** for [User Studies in Interactive Intelligent Systems](https://ekvv.uni-bielefeld.de/kvv_publ/publ/vd?id=450572659) at Bielefeld University.
= March 2024 =
    **Lecture Series** on [Machine Learning](https://interdisciplinary-college.org/2024-bc1/) at the [Interdisciplinary College (IK) 2024](https://interdisciplinary-college.org/).
= 2023-2024 =
    **Lecturer** for [Introduction to Data Mining](https://ekvv.uni-bielefeld.de/kvv_publ/publ/vd?id=426089509) at Bielefeld University.
= 2023-2024 =
    **Lecturer** for [Human-Centric Machine Learning](https://ekvv.uni-bielefeld.de/kvv_publ/publ/vd?id=406285531) at Bielefeld University.
= March 2023 =
    **Lecture Series** on [Machine Learning](https://interdisciplinary-college.org/2023-bc3/) at the [Interdisciplinary College (IK) 2023](https://interdisciplinary-college.org/).
= March 2021 =
    **Lecture Series** entitled [The AI Go Tournament](https://interdisciplinary-college.org/2021-pc3/) at the [Interdisciplinary College (IK) 2021](https://interdisciplinary-college.org/).
= 2020 =
    **Guest lecturer** at [The University of Sydney](https://www.sydney.edu.au) for [IT research methods](https://www.sydney.edu.au/units/INFO5993).
= March 2020 =
    **Lecture Series** on [Machine Learning](https://interdisciplinary-college.org/2020-ic1/) at the [Interdisciplinary College (IK) 2020](https://interdisciplinary-college.org/).
= March 2019 =
    **Tutorial Design and Management** for [Deep Learning](https://ekvv.uni-bielefeld.de/kvv_publ/publ/vd?id=135897891) at Bielefeld University.
= 2018-2019 =
    **Guest Lectures** at the [University of Groningen](http://rug.nl/) on Neural Networks and Pattern Recognition.
= 2018-2019 =
    **Tutorial Design and Management** for [Applied Optimization](https://ekvv.uni-bielefeld.de/kvv_publ/publ/vd?id=135889613) at Bielefeld University.
= March 2018 =
    **Lecture Series** at the [Interdisciplinary College (IK) 2018](https://interdisciplinary-college.org/previous-iks/) on Myoelectric Signal Processing and Pattern Recognition.
= May 2017 =
    **ERASMUS+ Guest Lectures** at the [University of Pisa](https://www.unipi.it/index.php/english) on Transfer Learning and Metric Learning.
= 2016-2017 =
    **Seminar** on [Applied Modelling](https://ekvv.uni-bielefeld.de/kvv_publ/publ/vd?id=70552292) at Bielefeld University.
= 2015-2018 =
    **Tutorial Design and Management** for [Theoretical Computer Science](https://ekvv.uni-bielefeld.de/kvv_publ/publ/vd?id=89703701) at Bielefeld University.
= 2015-2016 =
    **Guest Lectures** at the Interdisciplinary College (IK).
= Summer Term 2015 =
    **Tutorial Design and Management** for [Algorithms of Computer Science](https://ekvv.uni-bielefeld.de/kvv_publ/publ/vd?id=53619965) at Bielefeld University.

# Academic Functions

= June 2025 =
    **Co-Chair** for the Center for Interdisciplinary Research Workshop on AI and Autonomy in Higher Education.
= March to December 2025 =
    **Area chair** for the [International Conference on Neural Information Processing (NeurIPS) 2025](https://neurips.cc/).
= June 2024 to September 2025 =
    **Program Chair** for the [48th German Conference on Artificial Intelligence (KI2025)](https://ki2025.gi.de/).
= March 2025 =
    **Co-Chair** of the [Interdisciplinary College (IK) 2025](https://interdisciplinary-college.org/).
= May 2024 =
    Participated in [Dagstuhl seminar 24192 -  Generalization by People and Machines](https://www.dagstuhl.de/en/seminars/seminar-calendar/seminar-details/24192).
= March to December 2024 =
    **Area chair** for the [International Conference on Neural Information Processing (NeurIPS) 2024](https://neurips.cc/).
= January 2024 =
    Co-Founder of the [Network AI and digital autonomy in science and education](https://kiaubi.net/).
= September 2023 to July 2024 =
    Program Chair for the [International Conference on Educational Data Mining 2024](https://educationaldatamining.org/edm2024/) in Atlanta, Georgia, USA.
= March 2023 to today =
    Speaker of the **program committee** for the [Interdisciplinary College (IK)](https://interdisciplinary-college.org/).
= May 2022 to August 2024 =
    Member of the **Scientific Advisory Board** of the Project [KI4CoLearnET](https://www.ibbf.berlin/projekte/aktuelle-projekte/ki4colearnet.html).
= Jan 2022 to July 2022 =
    **Co-Lead** of the [Mentoring Program for the International Joint Conference on Neural Networks (IJCNN) 2022](https://wcci2022.org/ieee-cis-student-and-early-career-mentoring-program/).
= Oct 2021 =
    **Co-Chair** of the Special Session [Deep Learning for Graphs](https://www.esann.org/special-sessions#session4) at the [Europian Symposium on Artificial Neural Networks (ESANN) 2021](https://www.esann.org/).
= Sep 2021 =
    **Area chair** for the applied data science track at the [European Conference on Machine Learning (ECML) 2021](https://2021.ecmlpkdd.org/?page_id=1599).
= Jun 2021 =
    Member of the **program committee** for the [Educational Data Mining in Computer Science Education (CSEDM) Workshop](https://sites.google.com/ncsu.edu/csedm-workshop-edm21/home) at the [International Conference on Educational Data Mining](https://educationaldatamining.org/edm2021).
= Jun 2021 =
    **Co-Chair** of the Workshop [Computational Approaches to Creativity in Educational Technologies](https://sites.google.com/view/aied2021-creativity/) and member of the **program committee** for the [iTextbooks workshop](https://intextbooks.science.uu.nl/workshop2021/) at the [Artificial Intelligence in Education Conference](https://aied2021.science.uu.nl/).
= March 2021 =
    **Chair** of the [Interdisciplinary College (IK) 2021](https://interdisciplinary-college.org/).
= Dec 2020 to today =
    Member of the [IEEE task force on reservoir computing](https://sites.google.com/view/reservoir-computing-tf/).
= May 2020 to today =
    **Associate Editor** for the Journal [Neural Processing Letters](https://www.springer.com/journal/11063/editors).
= Feb 2020 to today =
    Member of the [IEEE task force on learning for structured data](https://www.learning4graphs.org/).
= Apr 2020 =
    Member of the **Technical Program Committee** for the [European Symposium on Artificial Neural Networks (ESANN) 2020](https://www.esann.org)
= March 2020 =
    **Conference Management** for the [Interdisciplinary College (IK) 2020](https://interdisciplinary-college.de/).
= Apr 2019 =
    **Chair** of the Special Session [Embeddings and Representation Learning for Structured Data](http://www.elen.ucl.ac.be/Proceedings/esann/esannpdf/es2019-4.pdf) at the [European Symposium on Artificial Neural Networks (ESANN) 2019](http://esann.org).
= March 2019 =
    **Conference Management** for the [Interdisciplinary College (IK) 2019](https://interdisciplinary-college.de/).

# Outreach & Press

= December 2024 =
    Open Video Series for ORCA.nrw on [AI for open educational resources](https://www.orca.nrw/blog/ki-kompetenzen-staerken-mit-orca-nrw/neue-videoreihe-zu-ki-und-oer-startet-mit-prof-dr-benjamin-paassen/)
= May 2024 to today =
    Podcast [Autonomie & Algorithmen - Freiheit im Zeitalter von KI](https://autonomie-algorithmen.letscast.fm/) with Christiane Attig, part of the [Year of Science 2024 "Freedom"](https://www.wissenschaftsjahr.de/2024/).
= June 2024 =
    Talk [AI and autonomy in higher education](https://festival.hfd.digital/de/sessions-2024/?id=628817) at the University:Future Festival.
= January 2024 =
    Blog Entry for Jan-Martin Wiarda: [Why universities should now host open-source language models](https://www.jmwiarda.de/2024/01/26/warum-hochschulen-jetzt-selbst-sprachmodelle-hosten-sollten/).
= February 2023 to today =
    **Invited talks** at several outreach events on ChatGPT and education, e.g. Verband der Privatschulen NRW, Weiterbildung Hessen e.V., Hessischer Volkshochschulverband, TU Braunschweig, Bundesschülerkonferenz, VHS Höxter, Diakonie Baden, VHS-Verband Mecklenburg-Vorpommern, and Landkreis Uelzen.
= July 2023 =
    [Interview with Deutsche Welle SHIFT](https://www.dw.com/de/shift-leben-in-der-digitalen-welt/video-66252443) on AI in education
= August 2022 =
    Featured by Matt Parker (aka standupmaths) in [two](https://www.youtube.com/watch?v=_-AfhLQfb6w) [videos](https://www.youtube.com/watch?v=c33AZBnRHks) for a graph-theoretic solution to finding five words with distinct letters an [documenting later, faster solutions](https://docs.google.com/spreadsheets/d/11sUBkPSEhbGx2K8ah6WbGV62P8ii5l5vVeMpkzk17PI/edit#gid=0).
= April 2022 =
    Founding member and member of the scientific advisory board for [StarCode e.V.](https://www.star-code.eu/).
= August 2021 =
    Participant of the Summer Academy on International Politics and AI of the [Academy for International Affairs NRW](https://www.aia-nrw.org/de/).
= August 2021 =
    Contribution to the [3Blue1Brown Summer of Math Exposition](https://www.youtube.com/watch?v=-LwmHk-iJoM).
= Apr 2016 =
    Presenter at the [Famelab](https://www.youtube.com/watch?v=we-4e9JWRCY) in Bielefeld.

# Reviews

Over [one hundred reviews](https://www.webofscience.com/wos/author/record/1168170) for indexed academic journals, as well as numerous reviews for top conferences, such as the [International Conference on Advances in Neural Information Processing (NeurIPS)](https://neurips.cc/), the [International Conference for Machine Learning (ICML)](https://icml.cc/), the [International Conference on Learning Representations (ICLR)](https://iclr.cc/), the [International Joint Conference on Artificial Intelligence](https://ijcai-22.org/), the [AAAI Conference on Artificial Intelligence (AAAI)](https://www.aaai.org/Conferences/AAAI/aaai.php), the [European Symposium on Artificial Neural Networks (ESANN)](http://esann.org), and the [International Conference on Artificial Neural Networks (ICANN)](https://e-nns.org/icanns/).

# Supervision

I am currently supervising a [team](https://www.uni-bielefeld.de/fakultaeten/technische-fakultaet/arbeitsgruppen/kml/team/index.xml) of two postdoctoral researchers, eight PhD students, and nine student assistants.

I supervised 22 bachelor's theses, 8 master's theses, one honors thesis (Sydney), and one research internship. Some of these theses resulted in peer-reviewed publications ([Rüttgers, Kuhl, and Paaßen, 2023](https://educationaldatamining.org/edm2024/proceedings/2024.EDM-short-papers.45/index.html); [Paaßen, Jensen, and Hammer, 2016](http://www.educationaldatamining.org/EDM2016/proceedings/paper_17.pdf); [Kummert et al., 2016](https://doi.org/10.1007/978-3-319-44781-0_30)).
