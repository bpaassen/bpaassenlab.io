<img class="right" alt="Image of Benjamin Paassen" src="BenjaminPaassen.jpg" width="200">

Welcome to the homepage of Benjamin Paaßen, Junior Professor for [Knowlegde Representation and Machine Learning](https://www.uni-bielefeld.de/fakultaeten/technische-fakultaet/arbeitsgruppen/kml/) at Bielefeld University and research affiliate at the [Educational Technology Lab](https://www.dfki.de/en/web/research/research-departments/educational-technology-lab/) at the [German Research Center for Artificial Intelligence](https://www.dfki.de/en/web/).
On this site, you can find my [CV](CV.html), some [datasets](Datasets.html) I recorded,
a list of my [publications](Publications.html), my [software packages](Software.html),
and my [teaching materials](Teaching.html).
In case you wish to contact me, please use my e-Mail:

eMail: bpaassen<span style="display:none">-anti-spam</span>@techfak.uni-bielefeld.de  
mastodon: [@bpaassen@bildung.social](https://bildung.social/@bpaassen)

My orcid is [0000-0002-3899-2450](https://orcid.org/0000-0002-3899-2450).

My research topics are, among other things:

  - Educational Data Mining and Artificial Intelligence for Education (e.g. in my [2021 JEDM paper](https://arxiv.org/abs/2103.11614))
  - Limitations of Large Language Models (e.g. in a [EMNLP 2024 paper](https://arxiv.org/abs/2210.04359) or our [2023 workshop](https://sites.google.com/view/sail-ws-llms/home))
  - Machine Learning for Structured Data (e.g. in my [ICLR 2021 paper](https://openreview.net/forum?id=dlEJsyHGeaL), my [2022 Machine Learning paper](https://doi.org/10.1007/s10994-022-06223-7), or my dissertation [dissertation](https://doi.org/10.4119/unibi/2935545))
  - Hand Prosthesis Control (e.g. in my [2019 IEEE TNSRE paper](https://pub.uni-bielefeld.de/record/2934458))

This page was generated using the brilliant [list site](https://gitlab.com/jspricke/jspricke.gitlab.io) static site generator by [Jochen Sprickerhof](https://jochen.sprickerhof.de/). Usage according to [GPLv3](https://choosealicense.com/licenses/gpl-3.0/).
