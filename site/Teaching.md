# Teaching Materials

For a full list of my teaching experience, please refer to my [CV](CV.html). Here, I include a list of creative commons licensed teaching materials from my prior teaching activities.

* For my 2023 seminar 'Human-Centric Machine Learning' you can find the slides [here](human_centric_ml_2023_04_03.pdf)
* For my 2020 IK Course ['Introduction to Machine Learning'](https://interdisciplinary-college.org/2020-ic1/), you can find the slides and homework files [here](ik_2020_intro_ml.zip); you can find the LaTeX source files [here](ik_2020_intro_ml_sources.zip); the first lecture is also available on [twitch](https://www.twitch.tv/videos/592041175).
* In winter terms 2018 and 2019, I participated in teaching 'Applied Optimization' at Bielefeld University. The lecture notes for this course can be found [here](https://pub.uni-bielefeld.de/record/2935200)
* In years 2012-2019, I participated in teaching 'Theoretical Computer Science' at Bielefeld University. The German (!) lecture notes for this course can be found [here](skript_GTI.pdf)
* In 2018, I published a tutorial on the [tree edit distance](https://arxiv.org/abs/1805.06869).
